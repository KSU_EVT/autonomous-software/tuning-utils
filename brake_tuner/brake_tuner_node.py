import rclpy
from rclpy.publisher import Publisher
from rclpy.subscription import Subscription
from rclpy.node import Node
from tkinter import * 

from geometry_msgs.msg import Point32
from geometry_msgs.msg import Quaternion


class BrakeTuner(Node):
    def __init__(self) -> None:
        super().__init__("brake_tuner_node")

        self.control_input_message: Point32 = Point32()
        self.status_msg: Quaternion = Quaternion()

        self.gui = TKinterGui(self)

        self.control_input_publisher: Publisher = self.create_publisher(Point32, "/voltron_vcu/control_in", 10)
        self.vcu_status_subscriber: Subscription = self.create_subscription(Quaternion, "/voltron_vcu/status", self.status_update, 10)

        self.create_timer(.1, self.gui.update)
        
        
    def status_update(self, msg: Quaternion):
        self.status_msg = msg

    def publish_control_input(self):
        self.control_input_publisher.publish(self.control_input_message)

    def update_control_input(self, speed: DoubleVar = None, steering: DoubleVar = None):
        if speed is not None:
            self.control_input_message.x = speed.get()

        if steering is not None:
            self.control_input_message.y = steering.get()

class TKinterGui():

    def __init__(self, node: BrakeTuner) -> None:
        self.root: Tk = Tk()
        self.root.title("Voltron Brake Tuner")
        self.root.geometry("600x400")

        self.root.grid_columnconfigure(0, weight=1)
        self.root.grid_columnconfigure(1, weight=1) 

        self.root.grid_rowconfigure(0, weight=1)
        self.root.grid_rowconfigure(1, weight=1)
        self.root.grid_rowconfigure(2, weight=1)
        self.root.grid_rowconfigure(3, weight=1)

        self.node: BrakeTuner = node

        self.brake_torque: DoubleVar = DoubleVar()
        self.measured_speed: DoubleVar = DoubleVar()
        self.measured_accel_estimate: DoubleVar = DoubleVar()
        self.commanded_accel_estimate: DoubleVar = DoubleVar()

        self.speed = DoubleVar()
        self.steering = DoubleVar()

        self.create_objects()

    def update(self):
        self.node.update_control_input(self.speed, self.steering)
        self.node.publish_control_input()

        self.brake_torque.set(float(f'{self.node.status_msg.w:.2f}'))
        self.measured_speed.set(float(f'{self.node.status_msg.x:.2f}'))
        self.measured_accel_estimate.set(float(f'{self.node.status_msg.y:.2f}'))
        self.commanded_accel_estimate.set(float(f'{self.node.status_msg.z:.2f}'))

        self.root.update()

    def kill_movement(self):
        self.speed.set(0)
        self.steering.set(0)

    def create_objects(self) -> None:

        steering_slider = Scale(self.root,
            variable = self.steering,
            from_ = -13,
            to = 13,
            resolution= 0.1,
            orient = HORIZONTAL,
            length=200,
            sliderlength=20
        ) 
        steering_slider.grid(row= 1, column= 0)
        
        steering_label = Label(self.root, text = "Steering")
        steering_label.grid(row= 2, column= 0)

        speed_slider = Scale(self.root,
            variable = self.speed,
            from_ = 15,
            to = 0,
            resolution= 0.1,
            orient = VERTICAL,
            length=200,
            sliderlength=20
        )
        speed_slider.grid(row= 1, column= 1)

        speed_label = Label(self.root, text = "Speed")
        speed_label.grid(row= 2, column= 1)

        kill_button = Button(self.root, text="Kill Movement", bg="red", fg="white", command=self.kill_movement)
        kill_button.grid(row=0, columnspan=2)

        frame = Frame(self.root)
        frame.grid(row=3, columnspan=2)

        state_label = Label(frame, text="Brake Torque:")
        state_label.grid(row=0, column=0, padx=10)
        state_val_label = Label(frame, textvariable=self.brake_torque)
        state_val_label.grid(row=1, column=0)

        rpm_label = Label(frame, text="Measured RPM:")
        rpm_label.grid(row=0, column=1, padx=10)
        rpm_val_label = Label(frame, textvariable=self.measured_speed)
        rpm_val_label.grid(row=1, column=1)

        measured_accel_label = Label(frame, text="Measured Accel Est.:")
        measured_accel_label.grid(row=0, column=2, padx=10)
        measured_accel_val_label = Label(frame, textvariable=self.measured_accel_estimate)
        measured_accel_val_label.grid(row=1, column=2)

        commanded_accel_label = Label(frame, text="Commanded Accel Est.:")
        commanded_accel_label.grid(row=0, column=3, padx=10)
        commanded_accel_label = Label(frame, textvariable=self.commanded_accel_estimate)
        commanded_accel_label.grid(row=1, column=3)

def main(args=None):
    rclpy.init(args=args)
    node = BrakeTuner()
    
    rclpy.spin(node)

    node.destroy_node()
    rclpy.shutdown()


if __name__ == "__main__":
    main()